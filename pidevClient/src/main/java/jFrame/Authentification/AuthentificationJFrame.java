package jFrame.Authentification;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import pidev.domain.Administrator;
import pidevDelegate.AdministratorDelegate;
import java.awt.Font;
import java.awt.Color;

/**
 * 
 * @author Fedi
 *
 */
public class AuthentificationJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFieldLogin;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AuthentificationJFrame frame = new AuthentificationJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AuthentificationJFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 286);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JLabel lblNewLabel = new JLabel("Login : ");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 36));

		JLabel lblPassword = new JLabel("Password : ");
		lblPassword.setFont(new Font("Times New Roman", Font.BOLD, 36));

		textFieldLogin = new JTextField();
		textFieldLogin.setFont(new Font("Times New Roman", Font.PLAIN, 28));
		textFieldLogin.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Times New Roman", Font.BOLD, 28));

		JButton btnSingin = new JButton("Sing In");
		btnSingin.setForeground(Color.BLUE);
		btnSingin.setFont(new Font("Times New Roman", Font.BOLD, 30));
		btnSingin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				List<Administrator> administrators = AdministratorDelegate
						.findAll();
				boolean ok = false;
				for (Administrator administrator : administrators) {
					if (administrator.getLogin().equals(
							textFieldLogin.getText())
							&& administrator.getPassword().equals(
									passwordField.getText())) {
						MenuJFrame l = new MenuJFrame(administrator);
						l.setVisible(true);
						setVisible(false);
						ok = true;
						break;
					}
				}
				if (ok == false) {
					JOptionPane.showMessageDialog(null,
							"Check your login or password !");
					textFieldLogin.setText("");
					passwordField.setText("");
				}
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane
				.setHorizontalGroup(gl_contentPane
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_contentPane
										.createSequentialGroup()
										.addGap(43)
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addComponent(
																				lblNewLabel)
																		.addGap(71))
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addComponent(
																				lblPassword)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)))
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.TRAILING,
																false)
														.addComponent(
																btnSingin,
																Alignment.LEADING,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																passwordField,
																Alignment.LEADING,
																GroupLayout.DEFAULT_SIZE,
																137,
																Short.MAX_VALUE)
														.addComponent(
																textFieldLogin,
																Alignment.LEADING,
																GroupLayout.DEFAULT_SIZE,
																158,
																Short.MAX_VALUE))
										.addContainerGap(34, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_contentPane
						.createSequentialGroup()
						.addGap(28)
						.addGroup(
								gl_contentPane
										.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel)
										.addComponent(textFieldLogin,
												GroupLayout.PREFERRED_SIZE, 39,
												GroupLayout.PREFERRED_SIZE))
						.addGap(31)
						.addGroup(
								gl_contentPane
										.createParallelGroup(Alignment.LEADING)
										.addComponent(passwordField,
												GroupLayout.PREFERRED_SIZE, 39,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblPassword))
						.addGap(18)
						.addComponent(btnSingin, GroupLayout.PREFERRED_SIZE,
								40, GroupLayout.PREFERRED_SIZE).addGap(39)));
		contentPane.setLayout(gl_contentPane);
	}
}
