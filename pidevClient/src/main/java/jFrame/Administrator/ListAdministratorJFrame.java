package jFrame.Administrator;

import jFrame.Authentification.MenuJFrame;

import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.JButton;

import pidev.domain.Administrator;
import pidevDelegate.AdministratorDelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author Fedi
 *
 */
public class ListAdministratorJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	Administrator ad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListAdministratorJFrame frame = new ListAdministratorJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ListAdministratorJFrame() throws HeadlessException {
		super();
	}

	/**
	 * Create the frame.
	 */
	public ListAdministratorJFrame(Administrator admin) {
		ad = admin;
		System.out.println(ad.getFirstName());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		table = new JTable();
		table.setModel(new AdminModel());

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddAdminJFrame a = new AddAdminJFrame(ad);
				a.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator administrator = new Administrator();
				administrator.setId_Admin((Integer) table.getValueAt(
						table.getSelectedRow(), 0));
				administrator.setName((String) table.getValueAt(
						table.getSelectedRow(), 1));
				administrator.setFirstName((String) table.getValueAt(
						table.getSelectedRow(), 2));
				administrator.setLogin((String) table.getValueAt(
						table.getSelectedRow(), 3));
				administrator.setPassword((String) table.getValueAt(
						table.getSelectedRow(), 4));

				UpdateAdminJFrame u = new UpdateAdminJFrame(administrator, ad);
				u.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Administrator administrator = new Administrator();
				administrator.setId_Admin((Integer) table.getValueAt(
						table.getSelectedRow(), 0));
				administrator.setName((String) table.getValueAt(
						table.getSelectedRow(), 1));
				administrator.setFirstName((String) table.getValueAt(
						table.getSelectedRow(), 2));
				administrator.setLogin((String) table.getValueAt(
						table.getSelectedRow(), 3));
				administrator.setPassword((String) table.getValueAt(
						table.getSelectedRow(), 4));
				AdministratorDelegate.deleteAdmin(administrator);
				ListAdministratorJFrame l = new ListAdministratorJFrame(ad);
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuJFrame m = new MenuJFrame(ad);
				m.setVisible(true);
				setVisible(false);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane
				.setHorizontalGroup(gl_contentPane
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_contentPane
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																table,
																GroupLayout.PREFERRED_SIZE,
																396,
																GroupLayout.PREFERRED_SIZE)
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addComponent(
																				btnAdd)
																		.addGap(18)
																		.addComponent(
																				btnUpdate)
																		.addGap(18)
																		.addComponent(
																				btnDelete)
																		.addGap(18)
																		.addComponent(
																				btnMenu)))
										.addContainerGap(18, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(
				Alignment.TRAILING)
				.addGroup(
						gl_contentPane
								.createSequentialGroup()
								.addContainerGap(21, Short.MAX_VALUE)
								.addComponent(table,
										GroupLayout.PREFERRED_SIZE, 174,
										GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addGroup(
										gl_contentPane
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(btnAdd)
												.addComponent(btnUpdate)
												.addComponent(btnDelete)
												.addComponent(btnMenu))
								.addGap(15)));
		contentPane.setLayout(gl_contentPane);
	}

}
