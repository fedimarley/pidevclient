package jFrame.Authentification;

import jFrame.Administrator.ListAdministratorJFrame;
import jFrame.Association.ListAssociationJFrame;
import jFrame.Competition.CompetitionList;
import jFrame.Document.ListDocumentJFrame;
import jFrame.Event.ListEventJFrame;
import jFrame.Matche.MatchList;
import jFrame.Player.ListPlayerJFrame;
import jFrame.Referee.ListRefereeJFrame;
import jFrame.RefereeTrainingSession.ListRefereeTrainingSessionJFrame;
import jFrame.Stadium.StadiumList;
import jFrame.Training.ListTrainingJFrame;
import jFrame.TrainingSession.ListTrainingSessionJFrame;
import jFrame.antiDoping.antidlistj;

import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;

import pidev.domain.Administrator;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

/**
 * 
 * @author Fedi
 *
 */
public class MenuJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Administrator administrator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuJFrame frame = new MenuJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MenuJFrame() {
		// TODO Auto-generated constructor stub
	}

	public MenuJFrame(Administrator admin) throws HeadlessException {

		administrator = admin;
		System.out.println(administrator.getLogin());
		setSize(691, 485);
		JButton btnAdministrateur = new JButton("Administrateur");
		btnAdministrateur.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				ListAdministratorJFrame l = new ListAdministratorJFrame(
						administrator);
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnPlayer = new JButton("Player");
		btnPlayer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListPlayerJFrame l = new ListPlayerJFrame();
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnDocument = new JButton("Document");
		btnDocument.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListDocumentJFrame l = new ListDocumentJFrame(administrator);
				l.setVisible(true);
				setVisible(false);

			}
		});

		JButton btnEvenet = new JButton("Event");
		btnEvenet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListEventJFrame l = new ListEventJFrame(administrator);
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnTraining = new JButton("Training");
		btnTraining.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListTrainingJFrame m = new ListTrainingJFrame(administrator);
				m.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnMatch = new JButton("Match");
		btnMatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MatchList l = new MatchList();
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnStadium = new JButton("Stadium");
		btnStadium.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StadiumList s = new StadiumList();
				s.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnReferee = new JButton("Referee");
		btnReferee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListRefereeJFrame l = new ListRefereeJFrame();
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnTrainingSession = new JButton("Training Session");
		btnTrainingSession.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListTrainingSessionJFrame l = new ListTrainingSessionJFrame();
				l.setVisible(true);
				setVisible(false);

			}
		});

		JButton btnRefereeTrainingSession = new JButton(
				"Referee Training Session");
		btnRefereeTrainingSession.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				ListRefereeTrainingSessionJFrame l = new ListRefereeTrainingSessionJFrame();
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnCompetition = new JButton("Competition");
		btnCompetition.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CompetitionList l = new CompetitionList();
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnAntidoping = new JButton("Anti-Doping");
		btnAntidoping.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				antidlistj l = new antidlistj();
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnAssociation = new JButton("Association");
		btnAssociation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ListAssociationJFrame l = new ListAssociationJFrame();
				l.setVisible(true);
				setVisible(false);
			}
		});

		JPanel panel = new JPanel();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addComponent(
																				btnAdministrateur,
																				GroupLayout.PREFERRED_SIZE,
																				168,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnPlayer,
																				GroupLayout.PREFERRED_SIZE,
																				148,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				btnReferee,
																				GroupLayout.PREFERRED_SIZE,
																				146,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnAntidoping,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE))
														.addComponent(
																panel,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addGroup(
																groupLayout
																		.createParallelGroup(
																				Alignment.TRAILING,
																				false)
																		.addGroup(
																				groupLayout
																						.createSequentialGroup()
																						.addComponent(
																								btnRefereeTrainingSession,
																								GroupLayout.PREFERRED_SIZE,
																								171,
																								GroupLayout.PREFERRED_SIZE)
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addComponent(
																								btnTrainingSession,
																								GroupLayout.PREFERRED_SIZE,
																								203,
																								GroupLayout.PREFERRED_SIZE)
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addComponent(
																								btnTraining,
																								GroupLayout.DEFAULT_SIZE,
																								GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE))
																		.addGroup(
																				Alignment.LEADING,
																				groupLayout
																						.createSequentialGroup()
																						.addComponent(
																								btnCompetition,
																								GroupLayout.PREFERRED_SIZE,
																								118,
																								GroupLayout.PREFERRED_SIZE)
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addComponent(
																								btnMatch,
																								GroupLayout.PREFERRED_SIZE,
																								139,
																								GroupLayout.PREFERRED_SIZE)
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addComponent(
																								btnStadium)
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addComponent(
																								btnAssociation,
																								GroupLayout.PREFERRED_SIZE,
																								108,
																								GroupLayout.PREFERRED_SIZE)
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addComponent(
																								btnDocument)
																						.addPreferredGap(
																								ComponentPlacement.RELATED)
																						.addComponent(
																								btnEvenet,
																								GroupLayout.PREFERRED_SIZE,
																								94,
																								GroupLayout.PREFERRED_SIZE))))
										.addContainerGap(
												GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																btnAdministrateur)
														.addComponent(btnPlayer)
														.addComponent(
																btnReferee)
														.addComponent(
																btnAntidoping))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																btnCompetition)
														.addComponent(btnMatch)
														.addComponent(
																btnStadium)
														.addComponent(
																btnAssociation)
														.addComponent(
																btnDocument)
														.addComponent(btnEvenet))
										.addGap(6)
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																btnRefereeTrainingSession)
														.addComponent(
																btnTrainingSession)
														.addComponent(
																btnTraining))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(panel,
												GroupLayout.PREFERRED_SIZE,
												335, GroupLayout.PREFERRED_SIZE)
										.addContainerGap(
												GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel
				.setIcon(new ImageIcon(
						"F:\\Mes Documents\\9raya\\4 GL 4\\2eme semestre\\Pi-Dev\\image\\Championnat-d-Afrique-de-Tennis-de-Table.jpg"));
		panel.add(lblNewLabel);
		getContentPane().setLayout(groupLayout);
	}
}
