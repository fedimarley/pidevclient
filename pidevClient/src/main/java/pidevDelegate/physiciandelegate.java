package pidevDelegate;

import java.util.List;

import pidev.domain.Physician;
import pidev.services.physician.PhysicianServiceEjbRemote;
import serviceLocator.ServiceLocator;




public class physiciandelegate {
	private static PhysicianServiceEjbRemote getRemote(){
		return (PhysicianServiceEjbRemote) ServiceLocator.getInstance().getProxy("pidev/PhysicianServiceEjb!pidev.services.physician.PhysicianServiceEjbRemote");
		
		
	}
	
	
	public static void addPhysician(Physician physician){
		getRemote().addPhysician(physician);
	}
	
	public static void updatePhysician(Physician physician){
		getRemote().updatePhysician(physician);
	}
	
	public static Physician findPhysicianById(int id){
		return getRemote().findPhysicianById(id);
	}
	
	public static void deletePhysician(Physician physician){
		getRemote().deletePhysician(physician);
	}
	
	public static List<Physician> findAll() {
		
		return getRemote().findAll();
	}
	


}
