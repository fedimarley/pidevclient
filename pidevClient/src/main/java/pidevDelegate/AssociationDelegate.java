package pidevDelegate;

import java.util.List;

import pidev.domain.Association;
import pidev.services.association.AssociationServiceEJBRemote;
import serviceLocator.ServiceLocator;

public class AssociationDelegate {
	public static AssociationServiceEJBRemote getRemote() {
		return (AssociationServiceEJBRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/AssociationServiceEJB!pidev.services.association.AssociationServiceEJBRemote");
	}
	
	public static void addAssociation(Association association) {
		getRemote().addAssociation(association);
	}
	
	public static void updateAssociation(Association association) {
		getRemote().updateAssociation(association);
	}
	
	public static void deleteAssociation(Association association) {
		getRemote().deleteAssociation(association);
	}
	
	public static Association findById(int id) {
		return getRemote().findByIdAssociation(id);		
	}
	
	public static List<Association> findAll() {
		return getRemote().findAll();
		
	}
	
	public static Association findByName(String name) {
		return getRemote().getAssociationByName(name);
	}
}
