package jFrame.Stadium;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Stadium;
import pidevDelegate.StadiumDelegate;

public class StadiumModel extends AbstractTableModel {

		List<Stadium> stadiums;
		String [] headers = {"id","name","region"};
		public StadiumModel() {
			
			stadiums = StadiumDelegate.findAllStadium();
					}
	@Override
	public int getRowCount() {
		
		return stadiums.size();
	}

	@Override
	public int getColumnCount() {
		
		return headers.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		switch (columnIndex) {
		case 0: return stadiums.get(rowIndex).getId_Staduim();
		case 1: return stadiums.get(rowIndex).getName();
		case 2: return stadiums.get(rowIndex).getRegion();
			
			

		default: return null;
			
		}
		
	}

}
