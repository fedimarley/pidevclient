package jFrame.Matche;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import pidev.domain.Competition;
import pidev.domain.Matche;
import pidev.domain.Referee;
import pidev.domain.Stadium;
import pidevDelegate.CompetitionDelegate;
import pidevDelegate.MatcheDelegate;
import pidevDelegate.RefereeDelegate;
import pidevDelegate.StadiumDelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.awt.Font;

public class AddMatch extends JFrame {

	List<Competition> lst;
	List<Referee> lstR;
	List<Stadium> lstS;

	private JPanel contentPane;
	private JTextField tex2;
	private JTextField tex1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddMatch frame = new AddMatch();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddMatch() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 591, 384);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		final JComboBox comboBox = new JComboBox();

		final JComboBox comboBox_1 = new JComboBox();

		final JComboBox comboBox_2 = new JComboBox();

		tex2 = new JTextField();
		tex2.setColumns(10);

		tex1 = new JTextField();
		tex1.setColumns(10);

		JLabel lblDate = new JLabel("Date");

		JLabel lblTime = new JLabel("Time");

		JLabel lblNewLabel = new JLabel("Competition");

		JLabel lblNewLabel_1 = new JLabel("Referee");

		JLabel lblStadium = new JLabel("Stadium");

		lst = CompetitionDelegate.findAllCompetitions();
		for (Competition competition : lst) {
			comboBox.addItem(competition.getId_Competition());
		}
		lstR = RefereeDelegate.findAll();
		for (Referee referee : lstR) {
			comboBox_1.addItem(referee.getFirstName());
		}

		lstS = StadiumDelegate.findAllStadium();
		for (Stadium stadium : lstS) {
			comboBox_2.addItem(stadium.getName());
		}

		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Matche m1 = new Matche();
				m1.setDate(tex1.getText());
				m1.setTime(tex2.getText());

				Competition c1 = lst.get(comboBox.getSelectedIndex());
				m1.setCompetition(c1);
				Referee r1 = lstR.get(comboBox_1.getSelectedIndex());
				m1.setReferee(r1);
				Stadium s1 = lstS.get(comboBox_2.getSelectedIndex());
				m1.setStadium(s1);
				MatcheDelegate.addMatche(m1);
				JOptionPane.showMessageDialog(null, "Successful");

			}
		});

		JButton btnReturn = new JButton("Return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				dispose();
				MatchList frame = new MatchList();
				frame.setVisible(true);
			}
		});
		
		JLabel lblAddMatch = new JLabel("Add Match");
		lblAddMatch.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(102)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblDate)
								.addComponent(lblTime)
								.addComponent(lblNewLabel)
								.addComponent(lblNewLabel_1)
								.addComponent(lblStadium))
							.addGap(37)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(btnValider)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnReturn))
								.addComponent(tex1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(tex2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(211)
							.addComponent(lblAddMatch)))
					.addContainerGap(233, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblAddMatch)
					.addGap(27)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(tex1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblDate))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(tex2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblTime))
					.addGap(33)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel))
					.addGap(27)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_1))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(comboBox_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblStadium))
					.addPreferredGap(ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnValider)
						.addComponent(btnReturn))
					.addGap(28))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
