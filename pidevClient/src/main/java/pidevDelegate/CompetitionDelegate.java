package pidevDelegate;

import java.util.List;

import pidev.domain.Competition;
import pidev.services.competition.CompetitionServiceEjbRemote;


public class CompetitionDelegate {
	
	private static CompetitionServiceEjbRemote getRemote(){
		CompetitionServiceEjbRemote cart;
		cart = (CompetitionServiceEjbRemote) serviceLocator.ServiceLocator
				.getInstance().getProxy("/pidev/CompetitionServiceEjb!pidev.services.competition.CompetitionServiceEjbRemote");
		return cart;
	}
	
	public static void addCompetition(Competition competition){
		
		getRemote().addCompetition(competition);
		}
	
	public static void updateCompetition(Competition competition){
		getRemote().updateCompetition(competition);
		}
	
	public static Competition finByIdCompetition(int Id_Competition){
		
		return getRemote().finByIdCompetition(Id_Competition);
	}
	
	public static void deleteCompetition(Competition competition){
		
		getRemote().deleteCompetition(competition);
	}
	
	public static List<Competition> findAllCompetitions(){
		return getRemote().findAllCompetitions();
	}


}
