package jFrame.Player;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Player;
import pidevDelegate.PlayerDelegate;

public class ListPlayerModel extends AbstractTableModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Player> players = new ArrayList<Player>();
	String [] headers = {"Identifiant","Name","First Name","Age","Career","grade","Association"};
	
	public ListPlayerModel() {
		players = PlayerDelegate.findAll();
	}

	@Override
	public int getRowCount() {
		return players.size();
	}

	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0 : return players.get(rowIndex).getId_Player();
		case 1 : return players.get(rowIndex).getName();
		case 2 : return players.get(rowIndex).getFirstName();
		case 3 : return players.get(rowIndex).getAge();
		case 4 : return players.get(rowIndex).getCareer();
		case 5 : return players.get(rowIndex).getGrade();
		case 6 : return players.get(rowIndex).getAssociation().getName();
		default: return null;
		}
	}

}
