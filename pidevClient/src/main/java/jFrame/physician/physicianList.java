package jFrame.physician;

import jFrame.antiDoping.antidlistj;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;

import pidev.domain.AntiDopingResponsible;
import pidev.domain.Physician;
import pidevDelegate.physiciandelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class physicianList extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	int h;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					physicianList frame = new physicianList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public physicianList(int id) {
		h = id;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 786, 516);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JButton btnReturn = new JButton("return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				new antidlistj().setVisible(true);
			}
		});

		JButton btnAdd = new JButton("add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AntiDopingResponsible a = new AntiDopingResponsible();
				a.setId_Responsible(h);
				a.setFirstName("dd");
				a.setName("dd");
				setVisible(false);
				new addPhysician(a).setVisible(true);
				
			}
		});

		JButton btnDelete = new JButton("delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AntiDopingResponsible a = new AntiDopingResponsible();
				a.setId_Responsible(h);
				a.setFirstName("dd");
				a.setName("dd");
				
				
				
				
				
				Physician p = new Physician();
				int id = (Integer.parseInt(table.getValueAt(
						table.getSelectedRow(), 0).toString()));
				String fname = table.getValueAt(table.getSelectedRow(), 1)
						.toString();
				String lname = table.getValueAt(table.getSelectedRow(), 2)
						.toString();
				p.setId_Physician(id);
				p.setFirstName(fname);
				p.setName(lname);
				p.setAntiDopingResponsible(a);
				physiciandelegate.deletePhysician(p);
				JOptionPane.showMessageDialog(null, "Deleted");
			}
		});

		JButton btnModify = new JButton("modify");
		btnModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AntiDopingResponsible a = new AntiDopingResponsible();
				a.setId_Responsible(h);
				a.setFirstName("dd");
				a.setName("dd");
				
				
				setVisible(false);
				
				Physician p = new Physician();
				int id = (Integer.parseInt(table.getValueAt(
						table.getSelectedRow(), 0).toString()));
				String fname = table.getValueAt(table.getSelectedRow(), 1)
						.toString();
				String lname = table.getValueAt(table.getSelectedRow(), 2)
						.toString();
				p.setId_Physician(id);
				p.setFirstName(fname);
				p.setName(lname);
				p.setAntiDopingResponsible(a);
				setVisible(false);
				new modifyPhysician(p).setVisible(true);

			}
		});

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		JLabel lblListPhysicians = new JLabel("list Physicians");
		lblListPhysicians.setFont(new Font("Tahoma", Font.BOLD, 22));
		table.setModel(new physicianModel(id));

		JLabel lblListPhysicians_1 = new JLabel("list physicians");
		lblListPhysicians_1.setFont(new Font("Tahoma", Font.BOLD, 21));
		
		JButton btnAdd_1 = new JButton("Add a test");
		btnAdd_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AntiDopingResponsible a = new AntiDopingResponsible();
				a.setId_Responsible(h);
				a.setFirstName("dd");
				a.setName("dd");
				
				Physician p = new Physician();
				int id = (Integer.parseInt(table.getValueAt(
						table.getSelectedRow(), 0).toString()));
				String fname = table.getValueAt(table.getSelectedRow(), 1)
						.toString();
				String lname = table.getValueAt(table.getSelectedRow(), 2)
						.toString();
				p.setId_Physician(id);
				p.setFirstName(fname);
				p.setName(lname);
				p.setAntiDopingResponsible(a);
				
				
				
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(41)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnModify)
						.addComponent(btnDelete)
						.addComponent(btnAdd)
						.addComponent(btnAdd_1))
					.addPreferredGap(ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(lblListPhysicians_1)
						.addComponent(table, GroupLayout.PREFERRED_SIZE, 381, GroupLayout.PREFERRED_SIZE))
					.addGap(87))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(679, Short.MAX_VALUE)
					.addComponent(btnReturn))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblListPhysicians_1)
					.addPreferredGap(ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(table, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnModify)
							.addGap(18)
							.addComponent(btnDelete)
							.addGap(18)
							.addComponent(btnAdd)
							.addGap(11)
							.addComponent(btnAdd_1)
							.addGap(21)))
					.addGap(48)
					.addComponent(btnReturn)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}

public physicianList() {
	// TODO Auto-generated constructor stub
}

}
