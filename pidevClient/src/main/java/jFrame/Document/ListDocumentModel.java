package jFrame.Document;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Document;
import pidevDelegate.DocumentDelegate;
/**
 * 
 * @author Fedi
 *
 */
public class ListDocumentModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Document> documents = new ArrayList<Document>();
	String [] headers = {"Identifiant","Information","Administrator"};
	
	public ListDocumentModel() {
		documents = DocumentDelegate.findAll();
	}

	public int getRowCount() {
		return documents.size();
	}

	public int getColumnCount() {
		return headers.length;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0: return documents.get(rowIndex).getId_Document();
		case 1 : return documents.get(rowIndex).getInformation();
		case 2 : return documents.get(rowIndex).getAdministrator().getLogin();
			
		default: return null;
		}
	}
		
}
