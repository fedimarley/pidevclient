package jFrame.RefereeTrainingSession;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Referee;
import pidev.domain.RefereeTrainingSession;
import pidev.domain.TrainingSession;
import pidevDelegate.RefereeDelegate;
import pidevDelegate.RefereeTrainingSessionServiceDelegate;
import pidevDelegate.TrainingSessionDelegate;

public class ListRefereeTrainingSessionModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<RefereeTrainingSession> trainings = new ArrayList<RefereeTrainingSession>();
	String [] headers = {"Identifiant Referee","Identifiant Session","Result Training"};
	
	public ListRefereeTrainingSessionModel() {
		// TODO Auto-generated constructor stub
		trainings = RefereeTrainingSessionServiceDelegate.findAll();
	}

	@Override
	public int getRowCount() {
		return trainings.size();
	
	}

	@Override
	public int getColumnCount() {
		return headers.length ;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0: return trainings.get(rowIndex).getReferee().getId_Referee();
		case 1: return trainings.get(rowIndex).getTrainingSession().getId_Session();	
		case 2: return trainings.get(rowIndex).getResultSession();
	
		
		default: return null;
		}
	}

	
	
}
