package pidevDelegate;

import java.util.List;

import pidev.domain.Administrator;
import pidev.domain.Training;
import pidev.services.administrator.AdministratorServiceEjbRemote;
import pidev.services.training.TrainingServiceEjbRemote;
import serviceLocator.ServiceLocator;

public class TrainingDelegate {
	public static TrainingServiceEjbRemote getRemote() {
		return (TrainingServiceEjbRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/TrainingServiceEjb!pidev.services.training.TrainingServiceEjbRemote");
	}
	
	public static void addTraining (Training training) {
		getRemote().addTraining(training);
		
	}
	
	public static void updateTraining (Training training) {
		getRemote().updateTraining(training);
	}
	
	public static void deleteTraining (Training training) {
		getRemote().deleteTraining(training);
	}
	
	public static Training findById(int id) {
		return getRemote().findByIdTraining(id);		
	}
	
	public static List<Training> findAll() {
		return getRemote().findAll();
		
	}
	

}
