package jFrame.Training;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Training;
import pidevDelegate.TrainingDelegate;

/**
 * 
 * @author Fedi
 *
 */
public class ListTrainingModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Training> trainings = new ArrayList<Training>();
	String[] headers = { "Identifiant", "Date", "Time", "Administrator" };

	public ListTrainingModel() {
		trainings = TrainingDelegate.findAll();
	}

	@Override
	public int getRowCount() {
		return trainings.size();
	}

	@Override
	public int getColumnCount() {
		return headers.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return trainings.get(rowIndex).getId_Training();
		case 1:
			return trainings.get(rowIndex).getDate();
		case 2:
			return trainings.get(rowIndex).getTime();
		case 3:
			return trainings.get(rowIndex).getAdministrator().getLogin();
		default:
			return null;
		}
	}

}
