package jFrame.antiDoping;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import pidev.domain.AntiDopingResponsible;
import pidevDelegate.antidopingdelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class antidopmodify extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	AntiDopingResponsible anti;
	private JButton btnReturn;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					antidopmodify frame = new antidopmodify();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
	}

	public antidopmodify() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Create the frame.
	 */
	public antidopmodify(AntiDopingResponsible a) {
		anti=a;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		textField_1 = new JTextField();
		textField_1.setText(anti.getFirstName());
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setText(anti.getName());
		textField_2.setColumns(10);
		
		JButton btnModify = new JButton("modify");
		btnModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				anti.setId_Responsible(anti.getId_Responsible());
				anti.setFirstName(textField_1.getText());
				anti.setName(textField_2.getText());
			antidopingdelegate.updateAntiDopingResponsible(anti);
			
			
			
			JOptionPane.showMessageDialog(null, "Modified");
			}
		});
		
		btnReturn = new JButton("return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				antidlistj m=new antidlistj();
				m.setVisible(true);
			}
		});
		
		JLabel lblFirstName = new JLabel("First Name");
		
		JLabel lblName = new JLabel("Name");
		
		JLabel lblModifyAntidoping = new JLabel("Modify AntiDoping");
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(btnModify)
									.addPreferredGap(ComponentPlacement.RELATED, 276, Short.MAX_VALUE)
									.addComponent(btnReturn))
								.addComponent(lblFirstName)
								.addComponent(lblName)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(169)
							.addComponent(lblModifyAntidoping)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(21)
					.addComponent(lblModifyAntidoping)
					.addGap(16)
					.addComponent(lblFirstName)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblName)
					.addGap(5)
					.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 71, Short.MAX_VALUE)
					.addComponent(btnModify)
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(228, Short.MAX_VALUE)
					.addComponent(btnReturn))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
