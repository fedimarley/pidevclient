package pidevDelegate;

import java.util.List;

import pidev.domain.Administrator;
import pidev.services.administrator.AdministratorServiceEjbRemote;
import serviceLocator.ServiceLocator;

public class AdministratorDelegate {
	public static AdministratorServiceEjbRemote getRemote() {
		return (AdministratorServiceEjbRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/AdministratorServiceEjb!pidev.services.administrator.AdministratorServiceEjbRemote");
	}
	
	public static void addAdmin(Administrator administrator) {
		getRemote().addAdministrator(administrator);
	}
	
	public static void updateAdmin(Administrator administrator) {
		getRemote().updateAdministrator(administrator);
	}
	
	public static void deleteAdmin(Administrator administrator) {
		getRemote().deleteAdministrator(administrator);
	}
	
	public static Administrator findById(int id) {
		return getRemote().findByIdAdministrator(id);		
	}
	
	public static List<Administrator> findAll() {
		return getRemote().findAll();
		
	}
	
	public static Administrator findByLogin(String login) {
		return getRemote().getAdministratorByLogin(login);
	}
}
