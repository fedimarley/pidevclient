package jFrame.Referee;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Referee;
import pidevDelegate.RefereeDelegate;

public class ListRefereeModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Referee> referees = new ArrayList<Referee>();
	String [] headers = {"Identifiant","Name","First Name","Grade"};
	
	public ListRefereeModel() {
		// TODO Auto-generated constructor stub
		referees = RefereeDelegate.findAll();
	}

	@Override
	public int getRowCount() {
		return referees.size();
	
	}

	@Override
	public int getColumnCount() {
		return headers.length ;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0: return referees.get(rowIndex).getId_Referee();
		case 1: return referees.get(rowIndex).getName();	
		case 2: return referees.get(rowIndex).getFirstName();
		case 3: return referees.get(rowIndex).getGrade();
		
		default: return null;
		}
	}

	
	
}
