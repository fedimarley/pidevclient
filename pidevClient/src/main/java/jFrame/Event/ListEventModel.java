package jFrame.Event;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Event;
import pidevDelegate.EventDelegate;

/**
 * 
 * @author Fedi
 *
 */
public class ListEventModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Event> events = new ArrayList<Event>();
	String[] headers = { "Identifiant", "Information", "Type", "Administrator" };

	public ListEventModel() {
		events = EventDelegate.findAll();
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return events.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return headers.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return events.get(rowIndex).getId_Event();
		case 1:
			return events.get(rowIndex).getInformation();
		case 2:
			return events.get(rowIndex).getType();
		case 3:
			return events.get(rowIndex).getAdministrator().getLogin();
		default:
			return null;
		}
	}

}
