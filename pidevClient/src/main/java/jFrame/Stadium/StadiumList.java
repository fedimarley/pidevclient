package jFrame.Stadium;

import jFrame.Authentification.MenuJFrame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;





import pidev.domain.Administrator;
import pidev.domain.Stadium;
import pidevDelegate.StadiumDelegate;




import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Font;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StadiumList extends JFrame {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StadiumList frame = new StadiumList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StadiumList() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 625, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		initDataBindings();
		
		JLabel lblListOfStadiums = new JLabel("List of stadiums");
		lblListOfStadiums.setFont(new Font("Tahoma", Font.PLAIN, 18));
		
		JButton btnAjouer = new JButton("Add");
		btnAjouer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Addstadium().setVisible(true);
			}
		});
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Stadium s1 = new Stadium();
				s1.setId_Staduim((Integer)table.getValueAt(table.getSelectedRow(), 0));
				s1.setName((String)table.getValueAt(table.getSelectedRow(), 1));
				s1.setRegion((String)table.getValueAt(table.getSelectedRow(), 2));
				ModifStadium up = new ModifStadium(s1);
				up.setVisible(true);
			}
		});
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Stadium s1 = new Stadium();
				int id=(Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString()));
				s1.setId_Staduim(id);
				s1.setName(table.getValueAt(table.getSelectedRow(), 1).toString());
				s1.setRegion(table.getValueAt(table.getSelectedRow(), 2).toString());
				StadiumDelegate.deleteStadium(s1);
				StadiumList sl = new StadiumList();
				sl.setVisible(true);
				setVisible(false);
				
			}
		});
		
		table = new JTable();
		table.setModel(new StadiumModel());
		
		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuJFrame l = new MenuJFrame(new Administrator());
				l.setVisible(true);
				setVisible(false);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(212)
							.addComponent(lblListOfStadiums))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(19)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(table, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(btnMenu)
									.addGap(18)
									.addComponent(btnDelete)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(btnUpdate)
									.addGap(18)
									.addComponent(btnAjouer)))))
					.addContainerGap(19, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(32)
					.addComponent(lblListOfStadiums)
					.addGap(18)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 203, GroupLayout.PREFERRED_SIZE)
					.addGap(39)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnAjouer)
						.addComponent(btnUpdate)
						.addComponent(btnDelete)
						.addComponent(btnMenu))
					.addContainerGap(14, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
	protected void initDataBindings() {

	}
}
