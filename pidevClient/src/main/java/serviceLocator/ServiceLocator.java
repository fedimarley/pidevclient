package serviceLocator;

import java.util.HashMap;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 * this class represents the service locator design pattern
 * 
 * @author Fedi
 *
 */
public class ServiceLocator {

	private Context context;
	private Map<String, Object> cache;
	private static ServiceLocator instance;

	/**
	 * private constructor for singleton
	 */
	private ServiceLocator() {
		cache = new HashMap<String, Object>();
		try {
			context = new InitialContext();
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * this method return a single instance
	 * 
	 * @return service locator instance
	 */
	public static ServiceLocator getInstance() {
		if (instance == null)
			instance = new ServiceLocator();
		return instance;
	}

	/**
	 * cette methode permet de tester si l'objet existe dans le cache si il
	 * existe il retourne l'objet directement au client depuis le cache si nn il
	 * importe l'objet depuis le serveur et le retourne au client
	 * 
	 * @param jndi
	 * @return
	 */
	public synchronized Object getProxy(String jndi) {
		Object object = null;
		if (cache.get(jndi) != null) {
			return cache.get(jndi);
		} else {
			try {
				object = context.lookup(jndi);
				if (object != null) {
					cache.put(jndi, object);
				}
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		return object;
	}
}
