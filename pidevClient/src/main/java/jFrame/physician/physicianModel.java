
package jFrame.physician;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;
import javax.swing.text.StyledEditorKit.ForegroundAction;

import pidev.domain.Physician;
import pidevDelegate.physiciandelegate;

public class physicianModel extends AbstractTableModel {
	List<Physician> phlist = new ArrayList<Physician>();
	List<Physician> listph=new ArrayList<Physician>();
	String [] headers = {"id","nom","lastName","foreign"};
	int h;

	
	public physicianModel(int id) {
		super();
		h=id;
		phlist=physiciandelegate.findAll();
		
		
		for (Physician physician : phlist) {
			if (physician.getAntiDopingResponsible().getId_Responsible()==h) {
				listph.add(physician);
			}
			
		}
		
	}
	
	
	
	

	@Override
	public int getRowCount() {
		return listph.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return headers.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0: return listph.get(rowIndex).getId_Physician();
		case 1: return listph.get(rowIndex).getFirstName();
		case 2: return listph.get(rowIndex).getName();
		case 3: return listph.get(rowIndex).getAntiDopingResponsible().getId_Responsible();
			


		default:return null;
			
		}
	}
	
}
