package jFrame.Matche;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Matche;
import pidevDelegate.MatcheDelegate;

public class MatchModel extends AbstractTableModel {
	
	List<Matche> matchs;
	String[] headers = {"id","date","time","idc","name","nameS"};
	
	public MatchModel() {
		matchs = MatcheDelegate.findAllMatch();
	}

	@Override
	public int getRowCount() {
		
		return matchs.size();
	}

	@Override
	public int getColumnCount() {
		
		return headers.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		switch (columnIndex) {
		case 0: return matchs.get(rowIndex).getId_Match();
		case 1: return matchs.get(rowIndex).getDate();
		case 2: return matchs.get(rowIndex).getTime();
		case 3: return matchs.get(rowIndex).getCompetition().getId_Competition();
		case 4: return matchs.get(rowIndex).getReferee().getName();
		case 5: return matchs.get(rowIndex).getStadium().getName();
			
			

		default: return null;
			
		}
	}

}
