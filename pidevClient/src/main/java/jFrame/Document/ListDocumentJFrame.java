package jFrame.Document;

import jFrame.Authentification.MenuJFrame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.JButton;

import pidev.domain.Administrator;
import pidev.domain.Document;
import pidevDelegate.AdministratorDelegate;
import pidevDelegate.DocumentDelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * 
 * @author Fedi
 *
 */
public class ListDocumentJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	Administrator administrator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListDocumentJFrame frame = new ListDocumentJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ListDocumentJFrame() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Create the frame.
	 */
	public ListDocumentJFrame(Administrator admin) {
		administrator = admin;
		System.out.println(administrator.getLogin());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 598, 377);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		table = new JTable();
		table.setModel(new ListDocumentModel());
		JButton btnNewButton_Add = new JButton("Add");
		btnNewButton_Add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddDocumentJFrame j = new AddDocumentJFrame(administrator);
				j.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnNewButton_Update = new JButton("Update");
		btnNewButton_Update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Document document = new Document();
				document.setId_Document((Integer)table.getValueAt(table.getSelectedRow(), 0));
				document.setInformation((String)table.getValueAt(table.getSelectedRow(), 1));
				Administrator admin = AdministratorDelegate.findByLogin((String)table.getValueAt(table.getSelectedRow(), 2));
				document.setAdministrator(admin);
				UpdateDocumentJFrame u = new UpdateDocumentJFrame(document,administrator);
				u.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnNewButton_Delete = new JButton("Delete");
		btnNewButton_Delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Document document = new Document();
				document.setId_Document((Integer)table.getValueAt(table.getSelectedRow(), 0));
				document.setInformation((String)table.getValueAt(table.getSelectedRow(), 1));
				Administrator admin = AdministratorDelegate.findByLogin((String)table.getValueAt(table.getSelectedRow(), 2));
				document.setAdministrator(admin);
				DocumentDelegate.deleteDocument(document);
				ListDocumentJFrame u = new ListDocumentJFrame(administrator);
				u.setVisible(true);
				setVisible(false);
			}
		});
		
		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuJFrame m = new MenuJFrame(administrator);
				m.setVisible(true);
				setVisible(false);
				
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(30)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnNewButton_Add)
							.addGap(18)
							.addComponent(btnNewButton_Update)
							.addGap(18)
							.addComponent(btnNewButton_Delete)
							.addGap(18)
							.addComponent(btnMenu))
						.addComponent(table, GroupLayout.PREFERRED_SIZE, 506, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(36, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(26)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE)
					.addGap(28)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_Add)
						.addComponent(btnNewButton_Update)
						.addComponent(btnNewButton_Delete)
						.addComponent(btnMenu))
					.addContainerGap(28, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
