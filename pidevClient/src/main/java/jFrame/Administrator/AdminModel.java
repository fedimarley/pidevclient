package jFrame.Administrator;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Administrator;
import pidevDelegate.AdministratorDelegate;
/**
 * 
 * @author Fedi
 *
 */
public class AdminModel extends AbstractTableModel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Administrator> administrators = new ArrayList<Administrator>();
	String [] headers = {"Identifiant","Name","First Name","Login","Password"};
	
	

	public AdminModel() {
		super();
		administrators = AdministratorDelegate.findAll();
	}

	public int getRowCount() {
		return administrators.size();
	}

	public int getColumnCount() {
		return headers.length;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0 : return administrators.get(rowIndex).getId_Admin();
		case 1 : return administrators.get(rowIndex).getName();
		case 2 : return administrators.get(rowIndex).getFirstName();
		case 3 : return administrators.get(rowIndex).getLogin();
		case 4 : return administrators.get(rowIndex).getPassword();
		default: return null;
		}
	}

}
