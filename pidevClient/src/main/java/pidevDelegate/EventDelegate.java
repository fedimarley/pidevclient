package pidevDelegate;

import java.util.List;

import pidev.domain.Event;
import pidev.services.event.EventServiceEjbRemote;
import serviceLocator.ServiceLocator;

public class EventDelegate {
	public static EventServiceEjbRemote getRemote() {
		return (EventServiceEjbRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/EventServiceEjb!pidev.services.event.EventServiceEjbRemote");
	}
	
	public static void addEvent(Event event) {
		getRemote().addEvent(event);
	}
	
	public static void updateEvent(Event event) {
		getRemote().updateEvent(event);
	}
	
	public static void deleteAdmin(Event event) {
		getRemote().deleteEvent(event);
	}
	
	public static Event findById(int id) {
		return getRemote().findByIdEvent(id);		
	}
	
	public static List<Event> findAll() {
		return getRemote().findAll();
		
	}
	
	

}
