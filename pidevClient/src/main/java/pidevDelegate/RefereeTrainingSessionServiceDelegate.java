package pidevDelegate;

import java.util.List;

import pidev.domain.RefereeTrainingSession;
import pidev.domain.TrainingSession;
import pidev.services.refereeTrainingSession.RefereeTrainingSessionEjbRemote;
import serviceLocator.ServiceLocator;


public class RefereeTrainingSessionServiceDelegate {

	public static RefereeTrainingSessionEjbRemote getRemote() {
		RefereeTrainingSessionEjbRemote cli;
		cli = (RefereeTrainingSessionEjbRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/RefereeTrainingSessionEjb!pidev.services.refereeTrainingSession.RefereeTrainingSessionEjbRemote");
		return cli;
	}
	/**
	 * this methode adds a Referee Training Session
	 * @param refereerainingSession
	 */
	public void addRefereeTrainingSession(RefereeTrainingSession refereeTrainingSession){
		
		getRemote().addRefereeTrainingSession(refereeTrainingSession);
	}
	/**
	 * this methode edit a Referee Training Session
	 * @param refereerainingSession
	 */
	public void updateRefereeTrainingSession(RefereeTrainingSession refereeTrainingSession)
	{
		getRemote().updateRefereeTrainingSession(refereeTrainingSession);
	}
	/**
	 * this methode find by id a Referee Training Session
	 * @param id
	 * @return
	 */
	public RefereeTrainingSession findRefereeTrainingSessionById(int id)
	{
		return null;
		
	}
	/**
	 * this methode remove Referee Training Session
	 * @param refereerainingSession
	 */
	public void deleteRefereeTrainingSession(RefereeTrainingSession refereerainingSession)
	{
		getRemote().deleteRefereeTrainingSession(refereerainingSession);
	}
	public static List<RefereeTrainingSession> findAll()
	{
		return getRemote().findAll();
	}
	
}
