package jFrame.antiDoping;

import jFrame.Authentification.MenuJFrame;
import jFrame.physician.physicianList;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import pidev.domain.Administrator;
import pidev.domain.AntiDopingResponsible;
import pidevDelegate.antidopingdelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTextField;

import java.awt.Font;

public class antidlistj extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	private JButton btnModify;
	private JButton btnAdd;
	private JTextField txtListAntidopingresponsible;
	private JButton btnMenu;

	

	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					antidlistj frame = new antidlistj();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public antidlistj() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 657, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		

		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				if (e.getClickCount() == 2 && !e.isConsumed()) {
				     e.consume();
				     int id=(Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString()));
				     System.out.println(id);
				     //setVisible(false);
						new physicianList(id).setVisible(true);
				}
				
				
				
				
			}
		});
		
		table.setModel(new antimodel());
		
		
		
		
		JButton btnDelet = new JButton("delete");
		btnDelet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AntiDopingResponsible a=new AntiDopingResponsible();
				int id=(Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString()));
				String fname= table.getValueAt(table.getSelectedRow(), 1).toString();
				String lname=table.getValueAt(table.getSelectedRow(), 2).toString();
				a.setId_Responsible(id);
				a.setFirstName(fname);
				a.setName(lname);
	
				antidopingdelegate.deleteAntiDopingResponsible(a);
				JOptionPane.showMessageDialog(null, "Deleted");
				setVisible(false);
			antidlistj m=new antidlistj();
			m.setVisible(true);
			}
		});
		
		btnModify = new JButton("modify");
		btnModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				AntiDopingResponsible a=new AntiDopingResponsible();
				int id=(Integer.parseInt(table.getValueAt(table.getSelectedRow(), 0).toString()));
				String fname= table.getValueAt(table.getSelectedRow(), 1).toString();
				String lname=table.getValueAt(table.getSelectedRow(), 2).toString();
				a.setId_Responsible(id);
				a.setFirstName(fname);
				a.setName(lname);
				new antidopmodify(a).setVisible(true);
			}
		});
		
		btnAdd = new JButton("add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new jAntiDoping().setVisible(true);
			}
		});
		
		txtListAntidopingresponsible = new JTextField();
		txtListAntidopingresponsible.setFont(new Font("Tahoma", Font.BOLD, 21));
		txtListAntidopingresponsible.setText("list AntidopingResponsible");
		txtListAntidopingresponsible.setColumns(10);
		
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuJFrame l = new MenuJFrame(new Administrator());
				l.setVisible(true);
				setVisible(false);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(btnModify, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnAdd, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnDelet, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addComponent(btnMenu))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(table, GroupLayout.PREFERRED_SIZE, 425, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(193)
							.addComponent(txtListAntidopingresponsible, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(125, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(txtListAntidopingresponsible, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(table, GroupLayout.PREFERRED_SIZE, 288, GroupLayout.PREFERRED_SIZE)
							.addGap(25))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(btnMenu)
							.addGap(26)
							.addComponent(btnAdd)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnModify)
							.addGap(28)
							.addComponent(btnDelet)
							.addGap(83))))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
