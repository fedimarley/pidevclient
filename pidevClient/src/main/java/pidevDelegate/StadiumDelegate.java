package pidevDelegate;

import java.util.List;

import pidev.domain.Stadium;
import pidev.services.stadium.StadiumServiceEjbRemote;


public class StadiumDelegate {
	
	private static StadiumServiceEjbRemote getRemote(){
		StadiumServiceEjbRemote cart;
		cart = (StadiumServiceEjbRemote) serviceLocator.ServiceLocator
				.getInstance().getProxy("/pidev/StadiumServiceEjb!pidev.services.stadium.StadiumServiceEjbRemote");
		return cart;
	}
	
	public static void addStadium(Stadium stadium){
		
		getRemote().addStadium(stadium);
		}
	
	public static void updateStadium(Stadium stadium){
		getRemote().updateStadium(stadium);
		}
	
	public static Stadium  findByIdStadium(int Id_Staduim){
		
		return getRemote().findByIdStadium(Id_Staduim);
	}
	
	public static void deleteStadium(Stadium stadium){
		
		getRemote().deleteStadium(stadium);
	}
	
	public static List<Stadium> findAllStadium(){
		return getRemote().findAllStadium();
	}


}
