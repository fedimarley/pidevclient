package jFrame.physician;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import pidev.domain.Physician;
import pidevDelegate.physiciandelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class modifyPhysician extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	Physician p;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					modifyPhysician frame = new modifyPhysician();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public modifyPhysician(Physician pp) {
		p=pp;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 726, 483);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblModifyPhysician = new JLabel("modify physician");
		lblModifyPhysician.setFont(new Font("Tahoma", Font.BOLD, 21));
		
		textField = new JTextField();
		textField.setText(p.getFirstName());
		//System.out.println(p.getAntiDopingResponsible());
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setText(p.getName());
		textField_1.setColumns(10);
		
		JButton btnModify = new JButton("modify");
		btnModify.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Physician p=new Physician();
				p.setId_Physician(p.getId_Physician());
				p.setFirstName(textField.getText());
				p.setName(textField_1.getText());
				p.setAntiDopingResponsible(p.getAntiDopingResponsible());
				physiciandelegate.updatePhysician(p);
				JOptionPane.showMessageDialog(null, "Modified");
				
			}
		});
		
		JButton btnReturn = new JButton("return");
		btnReturn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				physicianList m=new physicianList();
				m.setVisible(true);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(278)
							.addComponent(lblModifyPhysician))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(79)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnModify))))
					.addContainerGap(240, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(548, Short.MAX_VALUE)
					.addComponent(btnReturn)
					.addGap(31))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblModifyPhysician)
					.addGap(61)
					.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(58)
					.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(66)
					.addComponent(btnModify)
					.addPreferredGap(ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
					.addComponent(btnReturn)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
	
	public modifyPhysician() {
		// TODO Auto-generated constructor stub
	}
}
