package pidevDelegate;

import java.util.List;

import pidev.domain.Matche;
import pidev.services.matche.MatcheServiceEjbRemote;


public class MatcheDelegate {
	
	
	private static MatcheServiceEjbRemote getRemote(){
		MatcheServiceEjbRemote cart;
		cart = (MatcheServiceEjbRemote) serviceLocator.ServiceLocator
				.getInstance().getProxy("/pidev/MatcheServiceEjb!pidev.services.matche.MatcheServiceEjbRemote");
		return cart;
	}
	
	public static void addMatche(Matche match){
		
		getRemote().addMatche(match);
		}
	
	public static void updateMatche(Matche match){
		getRemote().updateMatche(match);
		}
	
	public static Matche findByIdMatch(int Id_Match){
		
		return getRemote().findByIdMatch(Id_Match);
	}
	
	public static void deleteMatch(Matche match){
		
		getRemote().deleteMatch(match);
	}

	public static List<Matche> findAllMatch(){
		return getRemote().findAllMatch();
	}


}
