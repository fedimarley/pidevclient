package pidevDelegate;

import java.util.List;

import pidev.domain.Administrator;
import pidev.domain.Referee;
import pidev.services.administrator.AdministratorServiceEjbRemote;
import pidev.services.referee.RefereeServiceEjbRemote;
import serviceLocator.ServiceLocator;

public class RefereeDelegate {
	public static RefereeServiceEjbRemote getRemote() {
		return (RefereeServiceEjbRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/RefereeServiceEjb!pidev.services.referee.RefereeServiceEjbRemote");
	}
	
	public static void addReferee(Referee referee) {
		getRemote().addReferee(referee);
	}
	
	public static void updateReferee(Referee referee) {
		getRemote().updateReferee(referee);
	}
	
	public static void deleteReferee(Referee referee) {
		getRemote().deleteReferee(referee);
	}
	
	public static Referee findById(int id) {
		return getRemote().findRefereeById(id);		
	}
	
	public static List<Referee> findAll() {
		return getRemote().findAll();
		
	}
}
