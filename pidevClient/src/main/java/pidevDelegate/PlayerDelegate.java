package pidevDelegate;

import java.util.List;

import pidev.domain.Player;
import pidev.services.player.PlayerServiceEjbRemote;
import serviceLocator.ServiceLocator;

public class PlayerDelegate {
	
	public static PlayerServiceEjbRemote getRemote() {
		return (PlayerServiceEjbRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/PlayerServiceEjb!pidev.services.player.PlayerServiceEjbRemote");
	}
	
	public static void addPlayer(Player player) {
		getRemote().addPlayer(player);
	}
	
	public static void updatePlayer(Player player) {
		getRemote().updatePlayer(player);
	}
	
	public static void deletePlayer(Player player) {
		getRemote().deletePlayer(player);
	}
	
	public static Player findById(int id) {
		return getRemote().findByIdPlayer(id);		
	}
	
	public static List<Player> findAll() {
		return getRemote().findAll();
		
	}

}
