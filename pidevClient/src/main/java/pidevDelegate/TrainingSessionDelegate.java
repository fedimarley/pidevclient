package pidevDelegate;

import java.util.List;

import pidev.domain.TrainingSession;
import pidev.services.trainingSession.TrainingSessionServiceEjbRemote;
import serviceLocator.ServiceLocator;

public class TrainingSessionDelegate {
	public static TrainingSessionServiceEjbRemote getRemote() {
		TrainingSessionServiceEjbRemote ts;
		ts = (TrainingSessionServiceEjbRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/TrainingSessionServiceEjb!pidev.services.trainingSession.TrainingSessionServiceEjbRemote");
		return ts;
	}
	/**
	 * this methode adds a Training Session
	 * @param TrainingSession
	 */
	public static void addTrainingSession(TrainingSession trainingSession)
	{
		getRemote().addTrainingSession(trainingSession);
	} 
	/**
	 * this methode edit a Training Session
	 * @param TrainingSession
	 */
	public static void updateTrainingSession(TrainingSession trainingSession)
	{
		getRemote().updateTrainingSession(trainingSession);
	} 
	/**
	 * this methode find by id a Training Session
	 * @param id
	 * @return
	 */
	public static TrainingSession findTrainingSessionById(int id){
		return getRemote().findTrainingSessionById(id);
		 
	}
	/**
	 * this methode remove a Training Session
	 * @param personne
	 */
	public static void deleteTrainingSession(TrainingSession trainingSession)
	{
		 
		getRemote().deleteTrainingSession(trainingSession);
	}
	public static List<TrainingSession> findAll()
	{
		return getRemote().findAll();
	}
}
