package jFrame.TrainingSession;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Referee;
import pidev.domain.TrainingSession;
import pidevDelegate.RefereeDelegate;
import pidevDelegate.TrainingSessionDelegate;

public class ListTrainingSessionModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<TrainingSession> trainings = new ArrayList<TrainingSession>();
	String [] headers = {"Identifiant","Date","Time","Admin"};
	
	public ListTrainingSessionModel() {
		// TODO Auto-generated constructor stub
		trainings = TrainingSessionDelegate.findAll();
	}

	@Override
	public int getRowCount() {
		return trainings.size();
	
	}

	@Override
	public int getColumnCount() {
		return headers.length ;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0: return trainings.get(rowIndex).getId_Session();
		case 1: return trainings.get(rowIndex).getDate();	
		case 2: return trainings.get(rowIndex).getTime();
		case 3: return trainings.get(rowIndex).getAdministrator().getId_Admin();
		
		default: return null;
		}
	}

	
	
}
