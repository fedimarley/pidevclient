package pidevDelegate;

import java.util.List;

import pidev.domain.AntiDopingResponsible;
import pidev.services.antiDopingResponsile.AntiDopingResponsibleServiceEjbRemote;
import serviceLocator.ServiceLocator;






public class antidopingdelegate {
	private static AntiDopingResponsibleServiceEjbRemote  getRemote(){
		return (AntiDopingResponsibleServiceEjbRemote ) ServiceLocator.getInstance().getProxy("/pidev/AntiDopingResponsibleServiceEjb!pidev.services.antiDopingResponsile.AntiDopingResponsibleServiceEjbRemote");
		
		
	}
	
	
	public static void addAntiDopingResponsible(AntiDopingResponsible antiDopingResponsible){
		getRemote().addAntiDopingResponsible(antiDopingResponsible);
	}

	public static void updateAntiDopingResponsible(AntiDopingResponsible antiDopingResponsible){
		getRemote().updateAntiDopingResponsible(antiDopingResponsible);
		}

	public static AntiDopingResponsible findAntiDopingResponsibleById(int id){
		return getRemote().findAntiDopingResponsibleById(id);}

	public static void deleteAntiDopingResponsible(AntiDopingResponsible antiDopingResponsible){
		getRemote().deleteAntiDopingResponsible(antiDopingResponsible);}
	
	public static List<AntiDopingResponsible> FindAll() {
		return getRemote().findAll();
		
	}
	


}
