package jFrame.Event;

import jFrame.Authentification.MenuJFrame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import pidev.domain.Administrator;
import pidev.domain.Event;
import pidevDelegate.AdministratorDelegate;
import pidevDelegate.EventDelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author Fedi
 *
 */
public class ListEventJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	Administrator administrator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListEventJFrame frame = new ListEventJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ListEventJFrame() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Create the frame.
	 */
	public ListEventJFrame(Administrator admin) {
		administrator = admin;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		table = new JTable();
		table.setModel(new ListEventModel());
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddEventJFrame a = new AddEventJFrame(administrator);
				a.setVisible(true);
				setVisible(false);

			}
		});

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Event event = new Event();
				event.setId_Event((Integer) table.getValueAt(
						table.getSelectedRow(), 0));
				event.setInformation((String) table.getValueAt(
						table.getSelectedRow(), 1));
				event.setType((String) table.getValueAt(table.getSelectedRow(),
						2));
				Administrator admin = AdministratorDelegate
						.findByLogin((String) table.getValueAt(
								table.getSelectedRow(), 3));
				event.setAdministrator(admin);
				UpdateEventJFrame u = new UpdateEventJFrame(event,
						administrator);
				u.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Event event = new Event();
				event.setId_Event((Integer) table.getValueAt(
						table.getSelectedRow(), 0));
				event.setInformation((String) table.getValueAt(
						table.getSelectedRow(), 1));
				event.setType((String) table.getValueAt(table.getSelectedRow(),
						2));
				Administrator admin = AdministratorDelegate
						.findByLogin((String) table.getValueAt(
								table.getSelectedRow(), 3));
				event.setAdministrator(admin);
				EventDelegate.deleteAdmin(event);
				ListEventJFrame l = new ListEventJFrame(administrator);
				l.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				MenuJFrame m = new MenuJFrame(administrator);
				m.setVisible(true);
				setVisible(false);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane
				.setHorizontalGroup(gl_contentPane
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_contentPane
										.createSequentialGroup()
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addGap(30)
																		.addComponent(
																				table,
																				GroupLayout.PREFERRED_SIZE,
																				365,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				btnAdd)
																		.addGap(18)
																		.addComponent(
																				btnUpdate)
																		.addGap(18)
																		.addComponent(
																				btnDelete)
																		.addGap(18)
																		.addComponent(
																				btnMenu)))
										.addContainerGap(29, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						gl_contentPane
								.createSequentialGroup()
								.addGap(31)
								.addComponent(table,
										GroupLayout.PREFERRED_SIZE, 140,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED,
										31, Short.MAX_VALUE)
								.addGroup(
										gl_contentPane
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(btnAdd)
												.addComponent(btnUpdate)
												.addComponent(btnDelete)
												.addComponent(btnMenu))
								.addGap(26)));
		contentPane.setLayout(gl_contentPane);
	}

}
