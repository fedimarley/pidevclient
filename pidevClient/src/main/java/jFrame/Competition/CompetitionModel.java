package jFrame.Competition;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Competition;
import pidevDelegate.CompetitionDelegate;

public class CompetitionModel extends AbstractTableModel {
	List<Competition> compts;
	String[] headers = {"id","endDate","startDate","name"};
	
	public CompetitionModel() {
		compts = CompetitionDelegate.findAllCompetitions();
	}

	@Override
	public int getRowCount() {
		return compts.size();
	}

	@Override
	public int getColumnCount() {
		
		return headers.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0: return compts.get(rowIndex).getId_Competition();
		case 1: return compts.get(rowIndex).getEndDate();
		case 2: return compts.get(rowIndex).getStartDate();
		case 3: return compts.get(rowIndex).getAdministrator().getLogin();
		
			

		default: return null;
	}
	}

}
