package pidevDelegate;

import java.util.List;

import pidev.domain.Document;
import pidev.services.document.DocumentServiceEjbRemote;
import serviceLocator.ServiceLocator;

public class DocumentDelegate {
	public static DocumentServiceEjbRemote getRemote() {
		return (DocumentServiceEjbRemote) ServiceLocator
				.getInstance()
				.getProxy(
						"/pidev/DocumentServiceEjb!pidev.services.document.DocumentServiceEjbRemote");
	}
	
	public static void addDocument(Document document) {
		getRemote().addDocument(document);
	}
	
	public static void updateDocument(Document document) {
		getRemote().updateDocument(document);
	}
	
	public static void deleteDocument(Document document) {
		getRemote().deleteDocument(document);
	}
	
	public static Document findById(int id) {
		return getRemote().findByIdDocument(id);		
	}
	
	public static List<Document> findAll() {
		return getRemote().findAll();
		
	}
}
