package jFrame.Association;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import pidev.domain.Association;
import pidev.domain.Referee;
import pidevDelegate.AssociationDelegate;
import pidevDelegate.RefereeDelegate;

public class ListAssociationModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<Association> associations = new ArrayList<Association>();
	String [] headers = {"Identifiant","Name"};
	
	public ListAssociationModel() {
		// TODO Auto-generated constructor stub
		associations = AssociationDelegate.findAll();
	}

	@Override
	public int getRowCount() {
		return associations.size();
	
	}

	@Override
	public int getColumnCount() {
		return headers.length ;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch (columnIndex) {
		case 0: return associations.get(rowIndex).getId_Association();
		case 1: return associations.get(rowIndex).getName();	
	
		default: return null;
		}
	}

	
	
}
