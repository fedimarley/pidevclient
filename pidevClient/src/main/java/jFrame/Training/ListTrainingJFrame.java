package jFrame.Training;

import jFrame.Authentification.MenuJFrame;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.JButton;

import pidev.domain.Administrator;
import pidev.domain.Training;
import pidevDelegate.AdministratorDelegate;
import pidevDelegate.TrainingDelegate;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author Fedi
 *
 */
public class ListTrainingJFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTable table;
	Administrator administrator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListTrainingJFrame frame = new ListTrainingJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ListTrainingJFrame() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Create the frame.
	 */
	public ListTrainingJFrame(Administrator admin) {
		administrator = admin;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		table = new JTable();
		table.setModel(new ListTrainingModel());
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddTrainingJFrame a = new AddTrainingJFrame(administrator);
				a.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Training training = new Training();
				training.setId_Training((Integer) table.getValueAt(
						table.getSelectedRow(), 0));
				training.setDate((String) table.getValueAt(
						table.getSelectedRow(), 1));
				training.setTime((String) table.getValueAt(
						table.getSelectedRow(), 2));
				Administrator admin = AdministratorDelegate
						.findByLogin((String) table.getValueAt(
								table.getSelectedRow(), 3));
				training.setAdministrator(admin);
				UpdateTrainingJFrame u = new UpdateTrainingJFrame(training,
						administrator);
				u.setVisible(true);
				setVisible(false);

			}
		});

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Training training = new Training();
				training.setId_Training((Integer) table.getValueAt(
						table.getSelectedRow(), 0));
				training.setDate((String) table.getValueAt(
						table.getSelectedRow(), 1));
				training.setTime((String) table.getValueAt(
						table.getSelectedRow(), 2));
				Administrator admin = AdministratorDelegate
						.findByLogin((String) table.getValueAt(
								table.getSelectedRow(), 3));
				training.setAdministrator(admin);
				TrainingDelegate.deleteTraining(training);
				ListTrainingJFrame u = new ListTrainingJFrame(administrator);
				u.setVisible(true);
				setVisible(false);
			}
		});

		JButton btnMenu = new JButton("Menu");
		btnMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuJFrame m = new MenuJFrame(administrator);
				m.setVisible(true);
				setVisible(false);
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane
				.setHorizontalGroup(gl_contentPane
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_contentPane
										.createSequentialGroup()
										.addGap(31)
										.addGroup(
												gl_contentPane
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_contentPane
																		.createSequentialGroup()
																		.addComponent(
																				btnAdd)
																		.addGap(18)
																		.addComponent(
																				btnUpdate)
																		.addGap(18)
																		.addComponent(
																				btnDelete)
																		.addGap(18)
																		.addComponent(
																				btnMenu))
														.addComponent(
																table,
																GroupLayout.PREFERRED_SIZE,
																355,
																GroupLayout.PREFERRED_SIZE))
										.addContainerGap(38, Short.MAX_VALUE)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						gl_contentPane
								.createSequentialGroup()
								.addGap(28)
								.addComponent(table,
										GroupLayout.PREFERRED_SIZE, 155,
										GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addGroup(
										gl_contentPane
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(btnAdd)
												.addComponent(btnUpdate)
												.addComponent(btnDelete)
												.addComponent(btnMenu))
								.addContainerGap(27, Short.MAX_VALUE)));
		contentPane.setLayout(gl_contentPane);
	}

}
